# sops-barbican

Helper image used by the argocd `repo-server`.

See further info under the `okd4-install` project > `chart\charts\argo-cd\values.yaml`.
