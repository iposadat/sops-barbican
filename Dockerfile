FROM alpine:3.13.1

LABEL maintainer="OpenShift Admins <openshift-admins@cern.ch>"

RUN apk add python3 py3-pip curl jq && \
    pip3 install --upgrade pip && \
    pip3 install yq && \
    pip3 cache purge && \
    # CERN Cloud SOPS solution info: https://gitlab.cern.ch/helm/releases/gitops-getting-started#secrets
    wget -O sops https://gitlab.cern.ch/cloud/sops/-/jobs/8834328/artifacts/raw/sops?inline=false && \
    mv --force ./sops /usr/local/bin && chmod +x /usr/local/bin/sops    
    
CMD ["/bin/sh"]
